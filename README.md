# MoonGameJam2021

Just some C# files used for MOONMOON GameJam 2021.

I am a total newbie to Unity Development. Last time I used Unity was around 2014 and 2015 while still in college for my Computer Science degree. I am however a web app developer (ASP MVC, .NET, SQL, etc), with around 8 years of programming experience.

This was a fun project to spend around 20 hours on the weekend developing a game jam idea and prototype from scratch with little knowledge of Unity, so this game is not very impressive, but was a lot of fun to make!


## Notes

5/28/2021: Brainstormed ideas about 1HP theme. Going with current idea. Fall back idea is a satirical MLG CoD/gaming Flappy Bird clone.

Ideas:

5/29/2021: Started on idea (MoonKing). Spent about 4-6 hours developing today.

- Menu/Level system
- Finetuning the jumping and mechanics of the game to fit the 1HP theme
